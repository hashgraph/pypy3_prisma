A portable version of pypy compiled with twisted, pymongo and pynacl. pypy gives amount 5x performance compared to cpython.

Tested on debian 8 64 bit system.

Steps

1. tar xvf pypy3_prisma_amd64.tar
2. sudo ln -s /home/directory/pypy3/bin/pypy3.5 /usr/local/bin/pypy3
3. pypy3 prismad.py --remoteport 8000 --remotenode 104.238.177.1 testing